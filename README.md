# tigase-swift

clone from <https://repository.tigase.org/git/tigase-swift>

## Changes

1. fix some bugs.
2. add [Carthage](https://github.com/Carthage/Carthage) support.